// SECTION  Comparison Query Operators

// $gt / $gte operator
	/*
	allows us to find documents that have field number values GREATER THAN or GREATER THAN OR EQUAL to a specified number

	SYNTAX
		db.collectionName.find({ field: { $gt: value}});
		db.collectionName.find({ field: { $gte: value}});
	
	*/
	
db.users.find({age: {$gt: 50}});
db.users.find({age: {$gte: 50}});



//  $lt / $lte operator
	/*
		LESS THAN / LESS THAN OR EQUAL operator

	SYNTAX
		db.collectionName.find({ field: { $lt: value}});
		db.collectionName.find({ field: { $lte: value}});
	
	*/


db.users.find({age: {$lt: 50}});
db.users.find({age: {$lte: 50}});



// $ne operator
	/*
	NOT EQUAL operator

	SYNTAX
		db.collectionName.find({field: {$ne: value}});
	*/


db.users.find({age: {$ne: 82}});



// $in operator
	/*
	allows us to find a document with a specific match criteria on one field using different values

	"includes"

	SYNTAX:
		db.collectionName.find({field:{$in: value}});
	*/


db.users.find({lastName: {$in: ["Hawking","Doe"]}});

db.users.find({courses: {$in: ["HTML","React"]}});


// SECTION  Logical Query Operators

// $or Operator
	/*
	allows us to match a single criteria from multiple provided search criteria
	
	SYNTAX:
		db.collectionName.find({$or: [{fieldA : valueA},{fieldB: valueB}, {fieldC: valueC}]});
	*/


db.users.find({ $or: [{firstName: "Niel"},{age: "21"}]});


// try to fuse $or and $gt operator
db.users.find({$or: [{firstName: "Jane"}, {age: {$gt: 21}}]});



// $and operator
	/*
	allows us to find documents matching multiple criteria in a single field.
	
	SYNTAX:
		db.collectionName.find({ $and: [{fieldA: valueA},{fieldB: valueB}]});
	*/


db.users.find({ $and: [{ age: {$ne: 82}}, { age: {$lt: 50}}] });




// SECTION Field Projection
	/*
	retrieving documents are common operations that we do and by default MongoDB queries return the whole document as a response.

	when dealing with complex data structures, there might be instances when fields are not useful for the query that we are trying to accomplish

	to help with the readability of the values returned, we can include/exclude fields from the response.

	in the query, we are trying to modify the fields shown to pull relevant data depending on the query
	*/

// INCLUSION
	/*
	SYNTAX
		db.collectionName.find({{criteria},{field: 1}})
	*/

db.users.find(
	{firstName: "Jane"},
	{
		firstName:  1,
		lastName: 1,
		contact: 1
	}
);

db.users.find(
	{ firstName: "Jane" },
	{
		firstName: true,
		lastName: true,
		contact: true
	}
);

// 1 = true




// EXCLUSION
	/*
	allows us to exclude specified field when making queries

	SYNTAX
		db.collectionName.find({{criteria},{field: 0}})
	*/


db.users.find(
	{ firstName: "Jane" },
	{
		contact: 0,
		department: 0
	}
);



// Returning Specific Fields in Embedded Documents
	/*
	

	*/	

// Inclusion
db.users.find(
	{ firstName: "Jane" },
	{
		contact: {
			phone: 1,
		}
	}
);

OR

db.users.find(
	{ firstName: "Jane" },
	{
		"contact.phone": 1,
	}
);


// Exclusion
db.users.find(
	{ firstName: "Jane" },
	{
		"contact.phone": 0
	}
);






// SECTION Project Specific Array Elements in Returned Array


// $slice operator
	/*
	allows us to retrieve only one element that matches the search criteria
	*/

db.users.find(
	{ "nameArr":{nameA: "Juan"} },
	{ "nameArr": {$slice : 1}}
);






// SECTIION	Evaluation Query Operators
// $regex
	/*
	allows us to find documents that match a specific string pattern using regular expressions

	SYNTAX:
		db.collectionName.find( { field: {$regex: "pattern", $options: $optionsValue} } );
	*/

// case-sensitive query
db.users.find( { firstName: {$regex: "N"} } );


// case-insensitive (with Options)
db.users.find( { firstName: {$regex: "j", $options: "$i"} } );